<?php

namespace App\Command;

use App\Service\TMDBServiceInterface;
use Meilisearch\Client;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:index-making', description: 'Create a new Index in MeiliSearch.')]
class GeneralIndexMakeCommand extends Command
{
    public function __construct(private readonly TMDBServiceInterface $apiService, private readonly Client $meiliSearchClient)
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Indexes data from a generic API into MeiliSearch.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $data = $this->apiService->fetchPopularMovies();
        $index = $this->meiliSearchClient->index('MoviesIndex');
        $index->addDocuments($data);

        $output->writeln('Data has been indexed in MeiliSearch.');

        return Command::SUCCESS;
    }

}