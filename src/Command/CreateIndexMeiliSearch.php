<?php

namespace App\Command;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Meilisearch\Client;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:create-meilisearch-index', description: 'Create a new Index in MeiliSearch.')]
class CreateIndexMeiliSearch extends Command
{
    /**
     * @param Client $meiliSearchClient
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        private readonly Client $meiliSearchClient,
        private readonly EntityManagerInterface $entityManager,
    )
    {
        parent::__construct();
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this->setDescription('Creates a new index in MeiliSearch.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $repository = $this->entityManager->getRepository(Person::class);
        $persons = $repository->findAll();

        $index = $this->meiliSearchClient->index('PersonIndex');

        $documents = [];
        foreach ($persons as $person) {
            $documents[] = [
                'objectID' => $person->getId(),
                'name' => $person->getName(),
            ];
        }

        $index->addDocuments($documents);

        $output->writeln('Indexation completed.');

        return Command::SUCCESS;
    }

}