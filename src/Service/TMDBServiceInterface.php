<?php

namespace App\Service;

interface TMDBServiceInterface
{
    /**
     * @return array
     */
    public function fetchPopularMovies(): array;}