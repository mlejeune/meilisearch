<?php

namespace App\Service;

use AllowDynamicProperties;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AllowDynamicProperties] class TMDBService implements TMDBServiceInterface
{
    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
        $this->httpClient = new Client([
            'base_uri' => 'https://api.themoviedb.org/3/',
        ]);
    }

    /**
     * @return array
     * @throws GuzzleException
     * @throws \JsonException
     */
    public function fetchPopularMovies(): array
    {
        $response = $this->httpClient->request('GET', 'movie/popular', [
            'query' => [
                'api_key' => $this->parameterBag->get('tmdb_api_key'),
                'language' => 'fr-FR',
            ]
        ]);

        $body = $response->getBody();

        $data = json_decode($body, true, 512, JSON_THROW_ON_ERROR);

        return $data['results'] ?? [];
    }
}