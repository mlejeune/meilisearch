<?php

namespace App\Controller;

use App\Entity\Person;
use App\Service\TMDBServiceInterface;
use Meilisearch\Bundle\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class MeiliSearchController extends AbstractController
{
    /**
     * @param Request $request
     * @param SearchService $searchService
     * @return Response
     */
    #[Route('/', name: 'app_meili_search')]
    public function search(Request $request, SearchService $searchService): Response
    {
        $searchTerm = $request->query->get('query', '');
        $rawHits = $searchService->rawSearch(Person::class, $searchTerm);

        return $this->render('meili_search/index.html.twig', [
            'hits' => $rawHits,
        ]);
    }

    #[Route('/movies', name: 'search_movies')]
    public function searchMovies(Request $request, TMDBServiceInterface $TMDBService): Response
    {
        $searchTerm = $request->query->get('query', '');
        $allMovies = $TMDBService->fetchPopularMovies();
        $filteredMovies = array_filter($allMovies, static function ($movie) use ($searchTerm) {
            return stripos($movie['title'], $searchTerm) !== false;
        });

        //How I Can Use directly Indexes to search movies


        return $this->render('movies/index.html.twig', [
            'movies' => $filteredMovies,
        ]);
    }
}
